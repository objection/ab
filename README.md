# AB

There's nothing much to this. You can assign a current one of a
"thing". A thing is a directory in ~/.local/share/ab with a file in
it, config, which can have only one line in it, "player", and then
some command, eg "mpv".

Here's my ~/.local/share/ab:

```txt
/home/neil/.local/share/ab
├── audiobook
│   ├── config
│   └── current
└── book
    ├── config
    └── current
```

## "config"

Here's "audiobook's" "config":

```txt
player mpv
```

And here's "book's":

```txt
player ebook-viewer
```

ebook-viewer is a Calibre-suite program; it just opens and ebook.

Note that this player's is passed as the first argument to execvp,
meaning it has to be just something like "mpv", not "mpv
--save-position-on-quit". However, since `ab` CDs to the "thing's"
directory, you can put a script in there and call that, like `player
./play.zsh`. Your script would might like:

```zsh
#!/usr/bin/env zsh

mpv --save-position-on-quit $@
```

## "current"

The "current" files, those are really a log of ones of "things" you
have opened through `ab`.

Here's the first 69 characters the of the last last three lines of my
"audiobook" "thing":

```txt
/home/neil/04-precursor-schraf/precursor-01-of-14.mp3|||/home/neil/04
/home/neil/mnt/non-essential/audiobooks/fiction/authors/rothfuss-patr
/home/neil/mnt/non-essential/audiobooks/fiction/authors/rothfuss-patr
```

You see I was listening to CJ Cherry, the third in her Foreigner
series. Then I listened to two Patrick Rothfuss books. I wonder which
they were? Answer: duh; how long has it been since the last one? More
than a decade.

You don't need to know this, but the "format" of each line of the
"current" file is just a list of files separated by "|||".

## Setting up / using

That's it. Set up your things by making a directory in ~/.share/ab,
and putting a "current" in there with a line that shows how to open
the kind of thing. When you want to read your thing -- or listen, or
read it, or whatever -- run `ab --thing=\<whatever\>`.

Well, first you'll want to tell `ab` what the latest thing is. Do that
with like this:

```bash
ab --thing=<your-thing> --set=<a glob of files>
```

Understand that _`ab`_ needs the glob, not the expanded files. It
needs something like this:

```bash
/home/me/audiobooks/bujold/curse-of-chalion/penrics-demon/*
```

Not, I dunno:

```bash
/home/me/audiobooks/bujold/curse-of-chalion/penrics-demon/1.mp3 /home/me/audiobooks/bujold/curse-of-chalion/penrics-demon/2.mp3 /home/me/audiobooks/bujold/curse-of-chalion/penrics-demon/3.mp3
```

The default thing is "audiobook". That's why the program is called
`ab`.



