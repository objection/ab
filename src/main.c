#define _GNU_SOURCE

#include <string.h>
#include "../lib/rab/rab.h"
#include "../lib/darr/darr.h"
#include "../lib/useful/useful.h"
#include "../lib/split/split.h"
#include "../lib/get-line/get-line.h"
#include "../lib/cat/cat.h"
#include "../lib/n-fs/n-fs.h"
#include <glob.h>
#include <unistd.h>
#include <err.h>
#include <argp.h>

#define DEFAULT_DATA_DIR "~/.local/share/ab"
#define CURRENT_THING_BASENAME "current"
#define CONFIG_NAME "config"

typedef struct strs strs_s;
$make_arr (char *, strs);

typedef struct config config_s;
struct config {
	char *player;
	char *copy_to;
};

typedef enum mode mode_e;
enum mode {
	M_PLAY,
	M_SET,
};

typedef struct args args_s;
struct args {
	mode_e mode;
	strs_s positionals;

	// This might be a glob.
	char *data_dir, *original_wd, *thing;
	bool print;
};

static void print (strs_s current_thing_files) {
	arr_each (&current_thing_files, file)
		printf ("%s\n", *file);
}

strs_s get_files_from_glob (char *glob_pattern, char *original_wd) {
	strs_s r = {};

	char *saved_dir = n_fs_chdir (original_wd);
	glob_t p_glob;
	int rt = glob (glob_pattern, GLOB_TILDE_CHECK, 0, &p_glob);
	if (rt && rt != 3) err (1, "glob failed");
	if (!p_glob.gl_pathc)
		errx (1, "No files match %s", glob_pattern);
	char resolved_path_buf[PATH_MAX];
	$fori (i, p_glob.gl_pathc) {
		realpath (p_glob.gl_pathv[i], resolved_path_buf);
		arr_add (&r, strdup (resolved_path_buf));
	}

	chdir (saved_dir);
	return r;
}

static strs_s get_files_from_stdin () {
	strs_s r = {};
	struct gl_line gl_line = make_line ("-", 0);

	while (get_line_strip (&gl_line) != -1)
		arr_add (&r, strdup (gl_line.buf));

	nuke_line (&gl_line);
	return r;

}

static void maybe_insert_files_got_from_stdin (strs_s *files) {
	bool have_got_files_from_stdin = 0;
	$fori (i, files->n) {
		auto file = files->d + i;
		if (**file == '-' && (*file)[1] == 0 && !have_got_files_from_stdin) {
			strs_s files_read_from_stdin = get_files_from_stdin ();

			// We insert the files. The idea is you can do this:
			// "ab -s cmdline-file.ogg - another-cmdline-file.ogg"
			// -- and the files would end up
			// in the right order. It occurs to me now you'd get
			// maximum flexibility if you had an -f option, which
			// would be a file with lines, and have the option that
			// "-". Then you could load from stdin *and* files *and*
			// pass files as arguments. But it's not important; I
			// won't do it.
			arr_insert_mul (files, i, files_read_from_stdin.d,
					files_read_from_stdin.n);
			i += files_read_from_stdin.n;
			arr_splice (files, i, 1);
			have_got_files_from_stdin = 1;
		}
	}
}

static int write_file_paths_to_current_file (strs_s *files, char *data_dir,
		bool should_print_files) {

	FILE *f = fopen (CURRENT_THING_BASENAME, "a");
	if (!f)
		err (1, "Couldn't open %s", CURRENT_THING_BASENAME);

	char *saved_dir = n_fs_chdir (data_dir);
	chdir (CURRENT_THING_BASENAME);

	arr_each (files, file) {
		fprintf (f, "%s", *file);

		// We'll use ||| for a separator, not 0x0, because the split
		// library didn't like that. Split should probably work with
		// zeroes, but I don't care right now.
		// 2023-06-17T09:54:09+01:00: but using these at least means
		// you can look at the file.
		if (file != files->d + files->n - 1)
			fprintf (f, "|||");
	}
	chdir (saved_dir);

	fprintf (f, "\n");
	fclose (f);

	if (should_print_files)
		print (*files);
	return 0;
}

static config_s get_config () {
	config_s r = {};
	struct gl_line gl_line = make_line (CONFIG_NAME, 0);
	if (!gl_line.f)
		err (1, "Couldn't open " CONFIG_NAME);

	int line = 1;
	while (get_line (&gl_line) != -1) {
		if (rab_is_all_x (gl_line.buf, strlen (gl_line.buf), isspace))
			continue;
		char *key = rab_get_first_non_ctype_sent (gl_line.buf, isblank);
		char *e = rab_get_first_ctype_sent (key, isblank);
		if (!e)
			errx (1, "%d: missing value", line);

		*e = 0;

		char *val = rab_get_first_non_ctype_sent (e + 1, isblank);
		e = strchr (val, '\n');
		while (isblank (*(e - 1))) e--;
		*e = 0;

		if ($strmatch (key, "player"))
			r.player = strdup (val);
		else if ($strmatch (key, "copy-to"))
			r.copy_to = n_fs_get_full_path (val);

		line++;
	}
	if (!r.player)
		errx (1, "Missing player in config. Put in something like \"player mpv\".");
	return r;
}

static strs_s get_current_thing_files (char *thing, char *data_dir) {

	strs_s r = {};
	char *buf;
	char *saved_dir = n_fs_chdir (data_dir);
	chdir (thing);

	if (cat_paths_into_buf (&buf, (const char *[]) {CURRENT_THING_BASENAME}, 1, 0))
		err (1, "Couldn't read %s's \"%s\" file", thing, CURRENT_THING_BASENAME);

	size_t n_lines;
	char **lines = split (buf, 0, (char *[]) { "\n" }, 1, &n_lines, 1);
	if (n_lines == 0)
		errx (1, "You have no current %s", thing);
	free (buf);
	char *line = lines[n_lines - 1];
	size_t n_sund;
	char **sund = split (line, 0, (char *[]) {"|||"}, 1, &n_sund, 1);
	$fori (i, n_sund) arr_add (&r, strdup (sund[i]));
	$free_arr (lines, n_lines);
	free (lines);
	chdir (saved_dir);
	return r;
}

static void print_current_things_files (char *match, char *data_dir) {
	auto current_thing_files = get_current_thing_files (match, data_dir);
	print (current_thing_files);
}

#define arr_dup(_arr) ({ \
	__typeof__ (*(_arr)) r = {}; \
	arr_each (_arr, elem) { \
		arr_add (&r, *elem); \
	} \
	r; \
})

static int play (char *match, char *data_dir, char *player, strs_s positionals) {
	auto current_thing_files = get_current_thing_files (match, data_dir);

	chdir (data_dir);
	chdir (match);

	// We dup the positionals to be defensive, you know.
	auto cmdline = arr_dup_alloc (&positionals);

	arr_insert (&cmdline, 0, player);
	arr_pusharr (&cmdline, current_thing_files.d, current_thing_files.n);
	arr_push (&cmdline, 0);

	setenv ("AB_THING", match, 1);
	setenv ("AB_THING_FILES", rab_join (current_thing_files.n,
				current_thing_files.d,
				&(rab_join_opts) {.glue = ","}), 1);
	execvpe (cmdline.d[0], cmdline.d, environ) != -1 ?: ({

			char *joined = rab_join (cmdline.n, cmdline.d,
					&(rab_join_opts) {.quotes = "\"", .glue = ", "});
			err (1, "executing %s failed", joined);
			});
	arr_each (&current_thing_files, _)
		free (*_);
	free (current_thing_files.d);
	/* if (opt_flags & OF_PLAY_NOW_EVEN_IF_SET) */
	/* 	err (1, "--now does nothing without --set"); */
	return 0;
}

char *match_thing (char *thing, char *data_dir) {

	char *r;

	glob_t pglob;

	// Note that ending a glob with a "/" matches directories.
	// I had no idea.
	int rt = glob ("*/", 0, 0, &pglob);
	if (rt && rt != 3) err (1, "glob failed");
	if (!pglob.gl_pathc) {
		errx (1, "You don't have any things defined (no directories in %s)",
				data_dir);
		return 0;
	}
	size_t n_matches = 1;
	char *matches[pglob.gl_pathc];

	rt = rab_get_longest_initial_matches (matches, &n_matches, thing,
			pglob.gl_pathc, pglob.gl_pathv, 0, 0);
	assert (!rt);
	switch (n_matches) {
		case 0: errx (1, "%s isn't a thing", thing); break;
		case 1: r = strdup (matches[0]); break;
		default:
			fprintf (stderr, "%s: --thing=%s matches too many things (",
					program_invocation_short_name, thing);
			rab_print_comma_list (n_matches, matches,
				&(struct rab_pcl_opts) {.f = stderr, .end = "\n"});
			exit (1);
			break;
	}
	globfree (&pglob);
	return r;
}

#if 0
static int get_copies_already_done (bool *pr, strs *files, char *copy_to_dir) {
	int r = 0;
	memset (pr, 0, sizeof *pr * files->n);
	if (!n_fs_fexists (copy_to_dir))
		return r;

	char buf[BUFSIZ];
	$fori (i, files->n) {

		// We assume that is like this:
		// 		orig: ~/this/that/HI.ogg.
		// 		new:  ~/Sync/HI.ogg.
		// If we were to allow copying directories this wouldn't be
		// enough.
		char *_basename = basename (files->d[i]);
		if (!_basename)
			err (1, "Couldn't get basename from %s", files->d[i]);
		snprintf (buf, BUFSIZ, "%s/%s", copy_to_dir, _basename);
		if (n_fs_fexists (buf)) {
			pr[i] = 1;
			r++;
		}
	}
	return r;
}
#endif

static int wipe_out_copy_to_dir_copy_to_and_modify_files_paths (strs_s *files, char *copy_to_dir) {
	int rc = n_fs_mkdir_p (copy_to_dir, 0750);
	if (rc)
		err (1, "Couldn't make the copy_to directory %s", copy_to_dir);

	n_fs_systemf ("rm %s/*", copy_to_dir);

	/* bool copies_already_done[files->n]; */
	/* get_copies_already_done (copies_already_done, files, copy_to_dir); */

	$fori (i, files->n) {
		/* if (copies_already_done[i]) */
		/* 	continue; */
		char *file_basename = basename (files->d[i]);
		char *new_path = 0;
		asprintf (&new_path, "%s/%s", copy_to_dir, file_basename);

		// Not checking that the file is valid, that it all got
		// copied, etc. That we skip copying matters when the files
		// are on different filesystems.
		int rc = n_fs_cp (files->d[i], new_path, 0);
		if (rc)
			err (1, "Couldn't copy %s to %s", files->d[i], new_path);

		free (files->d[i]);
		files->d[i] = new_path;
	}
	return 0;
}

static error_t parse_opt (int key, char *arg, struct argp_state *argp_state) {

	args_s *args = argp_state->input;

	switch (key) {
	case 's':
		args->mode = M_SET;
		break;
	case 'p':
		args->print = 1;
		break;
	case 't':
		args->thing = arg;
		break;
	case ARGP_KEY_INIT:
		break;
	case ARGP_KEY_ARG:
		arr_push (&args->positionals,

				// In some cases we'll free elements of this
				// array, so we'll strdup.
				strdup (arg));
		break;
	case ARGP_KEY_END:
		if (args->mode == M_SET && !args->positionals.n)
			errx (1, "-s needs args (paths)");
		if (!args->data_dir)
			args->data_dir = n_fs_get_full_path (DEFAULT_DATA_DIR);
		if (chdir (args->data_dir))
			err (1, "Couldn't cd to data dir %s", args->data_dir);
		if (!args->thing)
			args->thing = "audiobook";

	}
	return 0;
}

static int set (char *original_wd, char *copy_to, strs_s positionals, char *data_dir,
		char *thing, bool should_print) {
	chdir (original_wd);
	auto file_paths = arr_dup_alloc (&positionals);

	maybe_insert_files_got_from_stdin (&file_paths);

	if (copy_to)
		wipe_out_copy_to_dir_copy_to_and_modify_files_paths (&file_paths, copy_to);
	if (chdir (data_dir))
		err (1, "Couldn't chdir to %s", data_dir);
	if (chdir (thing))
		err (1, "Couldn't chdir to %s", thing);

	write_file_paths_to_current_file (&file_paths, data_dir, should_print);
	arr_each (&file_paths, _)
		free (*_);
	free (file_paths.d);
	return 0;
}

int main (int argc, char **argv) {
	struct argp argp = {
		.options = (struct argp_option []) {
			{"data-dir", 'd', "PATH", 0,
				"Use PATH as data-dir instead of XDG_DATA_HOME (usually ~/.local/share)"},
			{"player", 'P', "CMDLINE", 0,
				"The player (plus cmdline) to use"},
			{"print", 'p', 0, 0,
				"Print the current audiobook"},
			{"now", 'n', 0, 0,
				"If setting, play now; otherwise does nothing"},
			{"set", 's', 0, 0,
				"\
Set the current thing to ARGS; that is, this is a bool and its \"args\" are the \
positional parameters"},
			{"thing", 't', "STRING", 0,
				"\
The current thing to get. \"Things\" are directories inside ab's data dir, with \
a file called \"config\" (which right now should contain only \"player X\", where \
x is the program that will open the current thing), and \"current\", which is \
the file you put the paths of the current thing, either yourself or with ab's --set \
option."},
			{},
		},
		parse_opt,
		"<ARGS>", "\
Play the current audiobook with --player. Without -s, <ARGS> are extra args \
are passed to --player. With it, they're the files that are being registered"
	};

	args_s args = {
		.original_wd = getcwd (0, 0),
	};
	argp_parse (&argp, argc, argv, 0, 0, &args);

	char *thing_match = match_thing (args.thing, args.data_dir);
	if (chdir (thing_match))
		errx (1, "Can't chdir to thing %s", thing_match);

	auto config = get_config ();

	switch (args.mode) {
	case M_PLAY:
		if (args.print) {

			// This is really its own mode, but for whatever reason in
			// set we also, depending on the --print flag print the
			// files that we've just set in "set".
			print_current_things_files (thing_match, args.data_dir);
			break;
		}
		play (thing_match, args.data_dir, config.player, args.positionals);
		break;
	case M_SET:
		set (args.original_wd, config.copy_to, args.positionals, args.data_dir,
				thing_match, args.print);
		break;
	}

	exit (0);
}



